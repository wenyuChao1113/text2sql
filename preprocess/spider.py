import json
from transformers import T5Tokenizer
import torch
from torch.utils.data import Dataset
from tqdm import tqdm
import logging
from utils.str_util import normalize

logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')
logger = logging.getLogger(__name__)


class SpiderDataset(Dataset):
    def __init__(self, tokenizer, data_path, max_length, type):
        self.tokenizer = tokenizer
        self.max_length = max_length
        self.schema_file_path = data_path + "/" + "tables.json"
        if type == "train":
            self.file_path = data_path + "/" + "train_spider.json"
        elif type == "eval":
            self.file_path = data_path + "/" + "dev.json"

        self.table_schemas_mapping = {}
        self.inputs = []
        self.targets = []
        self.db_ids = []

        self._build()

    def __len__(self):
        return len(self.inputs)

    def _build(self):
        self._build_schema_mapping()
        self._build_inputs_targets()

    def _serialize_database(self, schema):
        db_str = " {db_id} | "
        table_str = "{table_name}: {columns}"
        table_sep = " | "
        column_str = "{column_name}"
        column_sep = " , "

        db_prefix = db_str.format(db_id=schema['db_id'])
        n_tables = len(schema['table_names'])
        col_with_type = [[col_item[0], col_item[1], type_item] for (col_item, type_item) in
                         zip(schema["column_names_original"], schema["column_types"])]

        table_str_list = []
        for i in range(n_tables):
            cols_of_single_table = list(filter(lambda x: x[0] == i, col_with_type))
            filled_col_str = column_sep.join(
                list(map(lambda x: column_str.format(column_name=x[1]), cols_of_single_table)))
            current_table_str = table_str.format(table_name=schema["table_names"][i], columns=filled_col_str)
            table_str_list.append(current_table_str)
        filled_table_str = table_sep.join(table_str_list)
        serialized_str = db_prefix + filled_table_str
        return serialized_str

    def _build_schema_mapping(self):
        logger.info("start building schema mapping")
        schemas = json.load(open(self.schema_file_path))

        for schema in schemas:
            schema_name = schema["db_id"]
            self.table_schemas_mapping[schema_name] = normalize(self._serialize_database(schema))

        logger.info("building schema mapping finished")

    def _build_inputs_targets(self):
        logger.info("start building inputs and targets")
        train_file = json.load(open(self.file_path))
        count = 0
        for line in tqdm(train_file):
            if 0:
                count += 1
                if count > 100:
                    break
            current_input = "{} | {}".format(line["question"], self.table_schemas_mapping[line["db_id"]])

            def formatSql(sql_string):
                return sql_string.replace(" . ", ".").replace("( ", "(").replace(" )", ")") \
                    .replace("count (", "count(")\
                    .replace("max (", "max(")\
                    .replace("min (", "min(")\
                    .replace("avg (", "avg(")\
                    .replace("value", "\"terminal\"")\
                    .replace("> =", ">=") \
                    .replace("< =", "<=") \
                    .replace("! =", "!=")

            current_output = formatSql(normalize(" ".join(line['query_toks_no_value'])))

            tokenized_input = self.tokenizer([current_input], max_length=self.max_length, padding="max_length",
                                             return_tensors="pt", truncation=True)
            tokenized_output = self.tokenizer([current_output], max_length=int(self.max_length / 2),
                                              padding="max_length",
                                              return_tensors="pt", truncation=True)

            self.db_ids.append(line["db_id"])
            self.inputs.append(tokenized_input)
            self.targets.append(tokenized_output)
        logger.info("building inputs and targets finsished!")
        return

    def __getitem__(self, idx):
        source_ids = self.inputs[idx].input_ids.squeeze()
        target_ids = self.targets[idx].input_ids.squeeze()

        src_mask = self.inputs[idx].attention_mask.squeeze()
        target_ids = torch.tensor([label if label != self.tokenizer.pad_token_id else -100 for label in target_ids])
        return {"input_ids": source_ids, "attention_mask": src_mask, "labels": target_ids}


if __name__ == '__main__':
    tokenizer = T5Tokenizer.from_pretrained("t5-base", cache_dir="../huggingface", local_files_only=True)

    dataset = SpiderDataset(tokenizer, "../origin_data/spider", max_length=4096, type="train")
    inputs = dataset.inputs
    outputs = dataset.targets
    temp = dataset[0]
    print("stop here")
