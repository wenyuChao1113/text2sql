import argparse
import logging

import torch
from torch.utils.data import DataLoader
from transformers import AdamW, T5ForConditionalGeneration, T5Tokenizer, get_scheduler

from preprocess.spider import SpiderDataset
from third_party.spider.evaluation import build_foreign_key_map_from_json, evaluate
from utils.frequent_util import get_time_stamp
from utils.save_reload_model import load_model, save_model
from tokenizers import AddedToken

device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')
logger = logging.getLogger(__name__)


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("--model_type", type=str, default="t5-base")
    parser.add_argument("--huggingface_cache_dir", type=str, default="./huggingface")
    parser.add_argument("--spider_data_path", type=str, default="./origin_data/spider")
    parser.add_argument("--sentence_max_length", type=int, default=512)
    parser.add_argument("--train_batch_size", type=int, default=2)
    parser.add_argument("--eval_batch_size", type=int, default=4)
    parser.add_argument("--epoches", type=int, default=20)
    parser.add_argument("--gradient_accumulation_steps", type=int, default=64)
    parser.add_argument("--max_train_steps", type=int, default=5000)

    parser.add_argument("--weight_decay", type=float, default=0.0)
    parser.add_argument("--learning_rate", type=float, default=5e-5)
    # parser.add_argument("--lr_scheduler_type", type=str, default="linear")
    # parser.add_argument("--num_warmup_steps", type=int, default=0)

    parser.add_argument("--checkpoint_dir", type=str, default="./checkpoints")
    parser.add_argument("--mode", type=str, default="train")

    parser.add_argument("--beam_size", type=int, default=5)
    parser.add_argument("--gold_eval_sql", type=str, default="./origin_data/spider/dev_gold.sql")
    parser.add_argument("--db_dir", type=str, default="./origin_data/spider/database")
    parser.add_argument("--table", type=str, default="./origin_data/spider/tables.json")
    parser.add_argument("--etype", type=str, default="match")

    args = parser.parse_args()
    return args


def train(model, data_loader, optimizer, current_epoch, tokenizer, args):
    model.to(device)
    completed_steps = 0
    accumulated_loss = 0
    for epoch in range(current_epoch + 1, args.epoches):
        model.train()
        for step, batch in enumerate(data_loader):
            input_ids = batch["input_ids"].to(device)
            attention_mask = batch["attention_mask"].to(device)
            labels = batch["labels"].to(device)

            outputs = model(input_ids=input_ids, attention_mask=attention_mask, labels=labels)
            loss = outputs.loss
            loss = loss / args.gradient_accumulation_steps
            accumulated_loss += loss.item()
            loss.backward()

            if step % args.gradient_accumulation_steps == 0 or step == len(data_loader) - 1:
                logger.info("epoch {}, step {}, loss: {}".format(epoch, step, accumulated_loss))
                optimizer.step()
                optimizer.zero_grad()
                completed_steps += 1
                accumulated_loss = 0
            if completed_steps >= args.max_train_steps:
                break
        save_model(model, epoch, completed_steps, "./checkpoints")
        generate_output(model, tokenizer, args)


def generate_output(model, tokenizer, args, output_dir="./output"):
    datasets = SpiderDataset(tokenizer, data_path=args.spider_data_path, max_length=args.sentence_max_length,
                             type="eval")
    data_loader = DataLoader(datasets, shuffle=False, batch_size=args.eval_batch_size)
    logger.info("start inferencing")
    output_path = "/".join([output_dir, get_time_stamp() + "output.txt"])
    results = []
    model.eval()
    model.to(device)
    for (idx, batch) in enumerate(data_loader):
        batch = batch["input_ids"].to(device)
        outputs = model.generate(batch, num_beams=args.beam_size)
        results += tokenizer.batch_decode(outputs, skip_special_tokens=True)
        logger.info("finished {}".format(idx))
    with open(output_path, "w") as f:
        for line in results:
            f.write("{}\n".format(line))
    logger.info("saved output to file")
    calculate_score(gold="./origin_data/spider/dev_gold.sql",
                    pred=output_path,
                    db_dir="./origin_data/spider/database",
                    table="./origin_data/spider/tables.json",
                    etype="match")
    return


def calculate_score(gold, pred, db_dir, table, etype):
    assert etype in ["all", "exec", "match"], "Unknown evaluation method"

    kmaps = build_foreign_key_map_from_json(table)

    evaluate(gold, pred, db_dir, etype, kmaps)


def main():
    args = parse_args()
    tokenizer = T5Tokenizer.from_pretrained(args.model_type, cache_dir=args.huggingface_cache_dir,
                                            local_files_only=True)
    tokenizer.add_tokens([AddedToken(" <"), AddedToken(" <=")])
    model = T5ForConditionalGeneration.from_pretrained(args.model_type, cache_dir=args.huggingface_cache_dir,
                                                       local_files_only=True)
    model.resize_token_embeddings(len(tokenizer))

    # Optimizer
    # Split weights in two groups, one with weight decay and the other not.
    no_decay = ["bias", "LayerNorm.weight"]
    optimizer_grouped_parameters = [
        {
            "params": [p for n, p in model.named_parameters() if not any(nd in n for nd in no_decay)],
            "weight_decay": args.weight_decay,
        },
        {
            "params": [p for n, p in model.named_parameters() if any(nd in n for nd in no_decay)],
            "weight_decay": 0.0,
        },
    ]
    optimizer = AdamW(optimizer_grouped_parameters, lr=args.learning_rate)

    model, optimizer, epoch = load_model(model, optimizer, args.checkpoint_dir)

    if args.mode == "eval":
        generate_output(model, tokenizer, args)
    elif args.mode == "train":
        datasets = SpiderDataset(tokenizer, data_path=args.spider_data_path, max_length=args.sentence_max_length,
                                 type=args.mode)
        dataloader = DataLoader(datasets, shuffle=True, batch_size=args.train_batch_size)
        train(model, dataloader, optimizer, epoch, tokenizer, args)


if __name__ == '__main__':
    main()
