import time


def get_time_stamp():
    now = int(round(time.time() * 1000))
    time_stamp = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(now / 1000))
    stamp = ("".join(time_stamp.split()[0].split("-")) + "".join(time_stamp.split()[1].split(":"))).replace('.', '')
    return stamp
