import re
import os
import torch
import logging

device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')
logger = logging.getLogger(__name__)


def get_latest_checkpoint_path(checkpoint_dir):
    pattern = re.compile(r'checkpoint_(.*?)\.pt', re.S)
    existing_checkpoint_epoch = list(map(lambda x: int(re.findall(pattern, x)[0]), os.listdir(checkpoint_dir)))
    if len(existing_checkpoint_epoch) != 0:
        latest_epoch = max(existing_checkpoint_epoch)
        path = checkpoint_dir + "/" + "checkpoint_" + str(latest_epoch) + ".pt"
        return path
    else:
        return None


def load_model(model, optimizer, checkpoint_dir):
    checkpoint_path = get_latest_checkpoint_path(checkpoint_dir)
    if checkpoint_path is not None:
        logger.info("start loading model from {}".format(checkpoint_path))
        checkpoint = torch.load(checkpoint_path, map_location=device)
        model.load_state_dict(checkpoint['model'])
        # optimizer.load_state_dict(checkpoint['optimizer'])
        epoch = checkpoint['current_epoch']
        # lr_scheduler.load_state_dict(checkpoint["lr_scheduler"])
        # for state in optimizer.state.values():
        #     for k, v in state.items():
        #         if torch.is_tensor(v):
        #             state[k] = v.to(device)
        logger.info("loading model from {} finished!".format(checkpoint_path))
        return model, optimizer, epoch
    else:
        return model, optimizer, 0


def save_model(model, current_epoch, current_train_steps, checkpoint_dir):
    existing_file = os.listdir(checkpoint_dir)
    for file in existing_file:
        os.remove(os.path.join(checkpoint_dir, file))
    logger.info("start saving model")
    checkpoint_name = "{}/checkpoint_{}.pt".format(checkpoint_dir, current_epoch)
    torch.save({
        "model": model.state_dict(),
        "current_epoch": current_epoch,
        "current_train_steps": current_train_steps
    }, checkpoint_name)
    logger.info("model saved to {}".format(checkpoint_name))
